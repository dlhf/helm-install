#!/bin/bash
helm install --namespace monitor --name prometheus apphub/prometheus --set alertmanager.persistentVolume.storageClass="nfs-client" --set server.persistentVolume.storageClass="nfs-client"
helm install  --namespace monitor --name grafana apphub/grafana --set persistence.enabled=true  --set persistence.storageClassName="nfs-client"
kubectl apply -f monitor-ingress.yaml
