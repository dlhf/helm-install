#!/bin/bash
kubectl create deployment nginx --image=nginx:alpine
kubectl create service clusterip nginx --tcp=80:80
kubectl create deployment tomcat --image=tomcat:8.5
kubectl create service clusterip tomcat --tcp=80:8080

cat <<EOF | kubectl apply -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx
spec:
  ingressClassName: nginx
  rules:
  - host: nginx.mac.cleanarch.cn
    http:
      paths:
      - backend:
          service:
            name: nginx
            port:
              number: 80
        path: /
        pathType: Prefix
EOF

cat <<EOF | kubectl apply -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: tomcat
spec:
  ingressClassName: nginx
  rules:
  - host: tomcat.mac.cleanarch.cn
    http:
      paths:
      - backend:
          service:
            name: tomcat
            port:
              number: 80
        path: /
        pathType: Prefix
EOF
