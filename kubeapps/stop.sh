#!/bin/bash
kubectl delete -f kubeapps-ingress.yaml
helm del --purge kubeapps
kubectl delete serviceaccount kubeapps-operator
kubectl delete clusterrolebinding kubeapps-operator
