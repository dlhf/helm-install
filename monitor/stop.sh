#!/bin/bash
helm del --purge grafana
helm del --purge prometheus
kubectl delete -f monitor-ingress.yaml
kubectl delete ns monitor
