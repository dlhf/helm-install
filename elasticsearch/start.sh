#!/bin/bash
kubectl apply -f https://download.elastic.co/downloads/eck/1.0.0-beta1/all-in-one.yaml

cat <<EOF | kubectl apply -f -
apiVersion: elasticsearch.k8s.elastic.co/v1beta1
kind: Elasticsearch
metadata:
  name: quickstart
spec:
  version: 7.5.1
  nodeSets:
  - name: default
    count: 3
    config:
      node.master: true
      node.data: true
      node.ingest: true
      node.store.allow_mmap: false
EOF

cat <<EOF | kubectl apply -f -
apiVersion: kibana.k8s.elastic.co/v1beta1
kind: Kibana
metadata:
  name: quickstart
spec:
  version: 7.5.1
  count: 1
  elasticsearchRef:
    name: quickstart
EOF

cat <<EOF | kubectl apply -f -
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: es
  namespace: default
spec:
  rules:
  - host: kibana.192.168.0.101.nip.io
    http:
      paths:
      - path: /
        backend:
          serviceName: quickstart-kb-http
          servicePort: 5601
  - host: elasticsearch.192.168.0.101.nip.io
    http:
      paths:
      - path: /
        backend:
          serviceName: quickstart-es-http
          servicePort: 9200
EOF
