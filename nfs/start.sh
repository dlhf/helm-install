#!/bin/sh
helm install nfs-client azure/nfs-client-provisioner --set nfs.server=192.168.0.130 --set nfs.path=/data/infra/nfs
kubectl patch storageclass nfs-client -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
