#!/bin/bash
kubectl taint nodes --all node-role.kubernetes.io/master-
helm repo add c7n https://openchart.choerodon.com.cn/choerodon/c7n/
helm repo add stable    http://mirror.azure.cn/kubernetes/charts/
helm repo add incubator http://mirror.azure.cn/kubernetes/charts-incubator/
helm repo add apphub    https://apphub.aliyuncs.com/
helm repo add elastic   https://helm.elastic.co
helm repo add azure    	http://mirror.azure.cn/kubernetes/charts/
helm repo add bitnami  	https://charts.bitnami.com/bitnami
helm repo add jetstack https://charts.jetstack.io
