#!/bin/bash

base_version=v1.17.0
ui_version=v1.8
pagoda_version=v1.2.0
registry_url=harbor.dlihaifeng.cn/wise2c

echo '开始tag镜像'

docker tag wise2c/yum-repo:$base_version $registry_url/yum-repo:$base_version
docker tag wise2c/playbook:$base_version $registry_url/playbook:$base_version
docker tag wise2c/deploy-ui:$ui_version $registry_url/deploy-ui:$ui_version
docker tag wise2c/pagoda:$pagoda_version $registry_url/pagoda:$pagoda_version

echo '开始推送镜像'

docker push $registry_url/yum-repo:$base_version
docker push $registry_url/playbook:$base_version
docker push $registry_url/deploy-ui:$ui_version
docker push $registry_url/pagoda:$pagoda_version

echo '开始删除原始镜像'

docker rmi wise2c/yum-repo:$base_version
docker rmi wise2c/playbook:$base_version
docker rmi wise2c/deploy-ui:$ui_version
docker rmi wise2c/pagoda:$pagoda_version
