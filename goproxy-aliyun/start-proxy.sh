#!/bin/bash
./proxy bridge -p ":33080" -C proxy.crt -K proxy.key --daemon --log proxy-bridge.log
./proxy server -r ":80@:8080" -P "127.0.0.1:33080" -C proxy.crt -K proxy.key --daemon --log proxy-server.log
