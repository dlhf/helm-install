#!/bin/bash
helm repo add nginx-stable https://helm.nginx.com/stable
helm install gateway nginx-stable/nginx-ingress -f values.yaml
