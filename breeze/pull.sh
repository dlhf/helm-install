#!/bin/bash

base_version=v1.17.0
ui_version=v1.8
pagoda_version=v1.2.0
registry_url=harbor.dlihaifeng.cn/wise2c

echo '开始拉取镜像'

docker pull $registry_url/yum-repo:$base_version
docker pull $registry_url/playbook:$base_version
docker pull $registry_url/deploy-ui:$ui_version
docker pull $registry_url/pagoda:$pagoda_version

echo '开始tag镜像'

docker tag $registry_url/yum-repo:$base_version wise2c/yum-repo:$base_version
docker tag $registry_url/playbook:$base_version wise2c/playbook:$base_version
docker tag $registry_url/deploy-ui:$ui_version wise2c/deploy-ui:$ui_version
docker tag $registry_url/pagoda:$pagoda_version wise2c/pagoda:$pagoda_version

echo '开始删除原始镜像'

docker rmi $registry_url/yum-repo:$base_version
docker rmi $registry_url/playbook:$base_version
docker rmi $registry_url/deploy-ui:$ui_version
docker rmi $registry_url/pagoda:$pagoda_version
