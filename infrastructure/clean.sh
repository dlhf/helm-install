#!/bin/sh
rm -rf data/elasticsearch/
rm -rf data/mongo/
rm -rf data/mysql/data
rm -rf data/nacos/standalone-logs
rm -rf data/rabbitmq/
rm -rf data/redis/data