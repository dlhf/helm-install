CREATE DATABASE jira;
CREATE DATABASE confluence;
CREATE DATABASE bitbucket;
CREATE DATABASE crowd;
CREATE DATABASE bamboo;
-- GRANT ALL PRIVILEGES ON DATABASE "jira" to atlassian;
-- GRANT ALL PRIVILEGES ON DATABASE "confluence" to atlassian;
-- GRANT ALL PRIVILEGES ON DATABASE "bitbucket" to atlassian;
-- GRANT ALL PRIVILEGES ON DATABASE "crowd" to atlassian;
